#include <iostream>
#include <chrono>

#define DEBUG(x) do { std::clog << x; } while (0);
#define STARTTIME() auto define_starttime = std::chrono::high_resolution_clock::now();
#define ENDTIME(point) auto define_endtime = std::chrono::high_resolution_clock::now(); DEBUG(point); DEBUG(": "); DEBUG(std::chrono::duration_cast<std::chrono::microseconds>(define_endtime-define_starttime).count()); DEBUG("\n") 