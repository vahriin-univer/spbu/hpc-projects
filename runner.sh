# run format: runner.sh main_file nodes ntasks

if [ $1 == "autoreg" ]; then
    touch autoreg/run.sh
    printf "#!/bin/bash\n" > autoreg/run.sh
    if [ $2 ]; then
        printf "#SBATCH --nodes=$2 --ntasks=$3\n" >> autoreg/run.sh
    fi
    printf "export OMP_NUM_THREADS=8\nmodule load mpi/openmpi-local\n./autoreg" >> autoreg/run.sh

    # send this folder to ant server
    rsync -a "$PWD" vahriin@ant.apmath.spbu.ru:~/

    # ssh running
    ssh vahriin@ant.apmath.spbu.ru "cd ~/hpc-projects/autoreg; make --quiet; chmod +x run.sh; srun run.sh"
else
    echo "no target"
fi

